import React, {Component} from "react"
import './App.css';
import {TicketContainer} from "./components/TicketBoard/TicketStore";
import ControlPanel from "./components/ControlPanel/ControlPanel";

class App extends Component {

    render() {
        return (
            <div className="container">
                <div className="row">
                    <ControlPanel />
                    <TicketContainer />
                </div>
            </div>
        );
    }
}

export default App;
