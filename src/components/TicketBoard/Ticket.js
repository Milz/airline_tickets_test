import React from "react"
import BtnBuy from "./BtnBuy";

const Ticket = ({stops, ...props}) => {
    let caseNumbers;
    if (stops === 0 || stops === 5) {
        caseNumbers = 'пересадок'
    } else if (stops === 1) {
        caseNumbers = 'пересадка'
    } else {
        caseNumbers = 'пересадки'
    }
    return (
        <div className="card card-ticket">
            <div className="card-body row">
                <div className="col-4">
                    <h5 className="card-title">Turkish airlines</h5>
                    <BtnBuy price={props.price}
                            currency={props.currency}
                            showForm={props.showForm}
                    />
                </div>
                <div className="col-8">
                    <div className="d-flex justify-content-between">
                        <span>{props.departure_time}</span>
                        <div className="d-flex flex-column justify-content-center">
                            <span>{stops + ' ' + caseNumbers}</span>
                            <span className="text-center">--></span>
                        </div>
                        <span>{props.arrival_time}</span>
                    </div>
                    <div className="d-flex justify-content-between">
                        <h6>{props.origin_name}</h6>
                        <h6>{props.destination_name}</h6>
                    </div>
                    <div className="d-flex justify-content-between">
                        <span>{props.departure_date}</span>
                        <span>{props.arrival_date}</span>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Ticket;