import React from "react"

const BtnBuy = ({price, currency, showForm}) => {

    return  <p className="btn btn-warning" onClick={() => {
        showForm()
    }}>Купить за {price}{currency}</p>
}

export default BtnBuy
