import React, {Component} from "react"
import Ticket from './Ticket';
import {connect} from "react-redux";
import {getFilteredTickets, getTickets} from "../../redux/tickets-reducer";
import {ModalContainer} from "../Modal/ModalContainer";
import {showForm} from "../../redux/modal-reducer";

class TicketStore extends Component {
    constructor(props) {
        super(props)
        this.props.getTickets();
    }

    componentDidMount() {
        this.props.getFilteredTickets();
    }

    render() {
        const ticketComponents = this.props.tickets.sort(function (a, b) {
            if (a.price > b.price) {
                return 1
            }
            if (a.price < b.price) {
                return -1
            }
            return 0
        })
            .map(ticket => <Ticket{...ticket} key={ticket.id} showForm={this.props.showForm}/>)
        return (
            <div className="col-8">
                {ticketComponents}
                <ModalContainer />
            </div>
        );
    }
}

let mapStateToProps = (state) => {
    return {
        tickets: state.ticketsBoard.filteredTickets
    }
}
export const TicketContainer = connect(mapStateToProps, {getTickets, getFilteredTickets, showForm})(TicketStore)


