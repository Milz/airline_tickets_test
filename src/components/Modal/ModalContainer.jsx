import React from "react"
import styles from './Modal.module.css'
import {connect} from "react-redux";
import {BtnCloseModalContainer} from "./BtnCloseModalContainer";
import FormPage from "./FormPage";
import SuccessPage from "./SuccessPage";
import {showSuccessPage} from "../../redux/modal-reducer";

const Modal = (props) => {
    return (
        <div>
            {(props.isFormShowed || props.isSuccessShowed) &&
            (<div className={styles.modal}>
                <div className={styles.modalBody}>
                    <BtnCloseModalContainer />
                    {props.isFormShowed ? <FormPage showSuccessPage={props.showSuccessPage}/> : <SuccessPage/>}
                </div>
            </div>)}
        </div>

    )
}
let mapStateToProps = (state) => {
    return {
        isFormShowed: state.modal.isFormShowed,
        isSuccessShowed: state.modal.isSuccessShowed
    }
}


export const ModalContainer = connect(mapStateToProps, {showSuccessPage})(Modal);