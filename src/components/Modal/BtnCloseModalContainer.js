import React from "react";
import {connect} from "react-redux";
import {closeModal} from "../../redux/modal-reducer";

const BtnCloseModal = ({closeModal}) =>  <p className="btn-close" onClick={() => closeModal()}>X</p>

export const BtnCloseModalContainer = connect(null, {closeModal})(BtnCloseModal);