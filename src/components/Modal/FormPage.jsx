import React from "react"
import {Field, reduxForm} from "redux-form";
import {email, maxLengthCreator, number, required} from "../../utils/validators";
import {Input} from "../common/FormControls/FormControls";

const maxLength30 = maxLengthCreator(30);
const maxLength15 = maxLengthCreator(15);
const LoginForm = ({handleSubmit}) => {
    return <form onSubmit={handleSubmit} className="form-group">
        <Field component={Input}
               name="email"
               placeholder="email"
               className="form-control"
               validate={[required, maxLength30, email]}/>
        <Field component={Input}
               name="phone"
               placeholder="phone"
               className="form-control"
               validate={[required, maxLength15, number]}/>
        <Field component={Input}
               name="firstname"
               placeholder="firstname"
               className="form-control"
               validate={[required, maxLength15]}/>
        <Field component={Input}
               name="secondname"
               placeholder="secondname"
               className="form-control"
               validate={[required, maxLength30]}/>
        <Field component={Input}
               name="passportnumber"
               placeholder="passportnumber"
               className="form-control"
               validate={[required, maxLength15]}/>
        <button
            className="btn btn-primary">Order
        </button>
    </form>
}

const ReduxLoginForm = reduxForm({
    // a unique name for the form
    form: 'loginForm'
})(LoginForm)

const FormPage = (props) => {
    const onSubmit = () => {
        props.showSuccessPage()
    }
    return (
        <div>
            <h2>Form</h2>
            <ReduxLoginForm onSubmit={onSubmit}/>
        </div>
    )
}

export default FormPage