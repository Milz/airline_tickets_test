import React from "react"
import styles from './FormControls.module.css'

export const Input = ({input, meta: {touched, error}, ...props}) => {
    const hasError = touched && error;
    return <div  className={styles.formControl + ' ' + (hasError ? styles.error: "")}>
        <div>
            <input {...input} {...props}/>
        </div>
        {hasError && <span>{error}</span>}
    </div>
}