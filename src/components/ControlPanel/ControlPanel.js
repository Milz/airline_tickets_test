import React from "react"
import StopsFormContainer from "./StopsForm/StopsFormContainer";
import CurrencyFormContainer from "./CurrencyForm/CurrencyFormContainer";

const ControlPanel = () => {
    return (
        <div className="col-4 control-panel">
            <h4>Валюта</h4>
            <CurrencyFormContainer />
            <h4>Колличество пересадок</h4>
            <StopsFormContainer />
        </div>
    )

}


export default ControlPanel