import React from "react"
import {connect} from "react-redux";
import StopsForm from "./StopsForm";
import {checkingStop} from "../../../redux/tickets-reducer";

const StopsFormContainer = (props) => {
    const onChangeStops = (stop) => {
        props.checkingStop(stop)
    }

    return <StopsForm
        onChange={onChangeStops}
        stops={props.stops}
        checkedStops={props.checkedStops}/>
}

let mapStateToProps = (state) => {
    return {
        stops: state.ticketsBoard.stops,
        checkedStops: state.ticketsBoard.checkedStops,
    }
}


export default connect(mapStateToProps, {checkingStop})(StopsFormContainer)