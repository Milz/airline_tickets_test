import React from "react"

const StopsForm = (props) => {

    let sortStops = props.stops.sort((a, b) => a - b);
    let checkboxes = sortStops.map((s, i) => {
        return <div className="form-check" key={i}>
            <label>
                <input
                    name={`stop ${s}`}
                    type="checkbox"
                    value={s}
                    className="form-check-input"
                    checked={props.checkedStops.includes(s)}
                    onChange={(e) => props.onChange(e.currentTarget.value)}
                />
                {s}
            </label>
        </div>
    })
    return (
        <form>
            {checkboxes}
        </form>
    )
}

export default StopsForm


