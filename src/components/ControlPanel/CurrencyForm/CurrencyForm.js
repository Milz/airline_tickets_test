import React from "react"
import {Field, reduxForm} from "redux-form";

const CurrencyForm = (props) => {
    let labelsRadio = props.currency.map(r => {
        return <label className={`btn btn-outline-primary ${r.active && 'active'}`}
                      key={r.id}>
            <Field
                name="currency"
                component="input"
                type="radio"
                value={r.name}
            />
            {r.name}
        </label>
    })

    return (
        <form onChange={props.onChange}>
            <div className="btn-group btn-group-toggle" data-toggle="buttons">
                {labelsRadio}
            </div>
        </form>
    )
}


export default reduxForm({
    // a unique name for the form
    form: 'currencyFiltersForm'
})(CurrencyForm)

