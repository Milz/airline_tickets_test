import React from "react"
import {connect} from "react-redux";
import CurrencyForm from "./CurrencyForm";
import {updateTicketsCurrency} from "../../../redux/tickets-reducer";

const CurrencyFormContainer = (props) => {
    const onChangeCurrency = (formData) => {
        if (formData.currency) {
            props.updateTicketsCurrency(formData.currency)
        }
    }

    return <CurrencyForm
        onChange={onChangeCurrency}
        currency={props.currency}/>
}

let mapStateToProps = (state) => {
    return {
        currency: state.ticketsBoard.currency
    }
}


export default connect(mapStateToProps, {updateTicketsCurrency})(CurrencyFormContainer)