import {applyMiddleware, combineReducers, createStore} from "redux";
import ticketsReducer from "./tickets-reducer";
import thunkMiddleware from "redux-thunk";
import {reducer as formReducer} from "redux-form";
import modalReducer from "./modal-reducer";


let reducers = combineReducers({
    ticketsBoard: ticketsReducer,
    modal: modalReducer,
    form: formReducer
})

let store = createStore(reducers, applyMiddleware(thunkMiddleware));
window.store = store;
export default store