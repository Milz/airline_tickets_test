import {ticketsApi} from "../api/api";
import * as types from './actionTypes';

let initialState = {
    tickets: [],
    filteredTickets: [],
    stops: ['all'],
    checkedStops: ['all'],
    currency: [
        {id: 1, name: 'USD', value: 27, active: false},
        {id: 2, name: 'EUR', value: 30, active: false},
        {id: 3, name: 'UAH', value: 1, active: true}
    ]
}

const ticketsReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.SET_TICKETS:
            const initialStops = Array
                .from(new Set(action.tickets.map(t => String(t.stops))));
            return {
                ...state,
                tickets: action.tickets.map(t => {
                    return {...t, basePrice: t.price, currency: "UAH"}
                }),
                stops: [...state.stops, ...initialStops]
            }
        case types.UPDATE_ACTIVE_CURRENCY:
            return {
                ...state,
                tickets: state.tickets.map(t => {
                    let selectedCurrency = state.currency.find(v => {
                        return v.name === action.currency
                    })
                    return {
                        ...t,
                        price: (t.basePrice * selectedCurrency.value),
                        currency: action.currency
                    }
                }),
                currency: state.currency.map(c => {
                    return {
                        ...c,
                        active: (c.name === action.currency)
                    }
                })
            }
        case types.UPDATE_CHECKED_STOPS:
            return {
                ...state,
                checkedStops:
                    state.checkedStops.includes(action.stop)
                        ? state.checkedStops.filter(stop => stop !== action.stop)
                        : (action.stop === 'all') ? ['all'] : [...state.checkedStops, action.stop].filter(stop => stop !== 'all')
            }
        case types.CHECKING_STOPS_LENGTH:
            const maxLengthCheckedStops = state.stops.length - 1;
            if (state.checkedStops.length === maxLengthCheckedStops) {
                return {
                    ...state,
                    checkedStops: ['all']
                }
            }
            return state;
        case types.SET_FILTERED_TICKETS:
            let allTickets = state.tickets;
            let checkedStops = state.checkedStops;
            return {
                ...state,
                filteredTickets: allTickets.filter(ticket => {
                    return (checkedStops.some(stop => ticket.stops == stop || 'all' === stop))
                })
            }

        default:
            return state;
    }
}

export const setTickets = (tickets) => ({type: types.SET_TICKETS, tickets});
const updateActiveCurrency = (currency) => (
    {type: types.UPDATE_ACTIVE_CURRENCY, currency});
const checkingStops = () => (
    {type: types.CHECKING_STOPS_LENGTH});
const updateCheckedStops = (stop) => ({type: types.UPDATE_CHECKED_STOPS, stop});
const setFilteredTickets = () => ({type: types.SET_FILTERED_TICKETS});

export const updateTicketsCurrency = (currency) => (dispatch) => {
    dispatch(updateActiveCurrency(currency))
    dispatch(setFilteredTickets())
}

export const checkingStop = (stop) => (dispatch) => {
    dispatch(updateCheckedStops(stop));
    dispatch(checkingStops());
    dispatch(setFilteredTickets())
}

export const getTickets = () => {
    return (dispatch) => {
        dispatch(setTickets(ticketsApi.getTickets()))
    }
}

export const getFilteredTickets = () => {
    return (dispatch) => {
        dispatch(setFilteredTickets())
    }
}

export default ticketsReducer