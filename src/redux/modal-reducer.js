import * as types from './actionTypes';

let initialState = {
    isFormShowed: false,
    isSuccessShowed: false
}

const modalReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.CLOSE_MODAL:
            return {
                ...state,
                isFormShowed: false,
                isSuccessShowed: false
            }
        case types.SHOW_FORM:
            return {
                ...state,
                isFormShowed: true,
                isSuccessShowed: false
            }
        case types.SHOW_SUCCESS:
            return {
                ...state,
                isFormShowed: false,
                isSuccessShowed: true
            }
        default:
            return state;
    }
}

export const closeModal = () => ({type: types.CLOSE_MODAL});
export const showForm = () => ({type: types.SHOW_FORM});
export const showSuccessPage = () => ({type: types.SHOW_SUCCESS});

export default modalReducer