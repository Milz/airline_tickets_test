export const required = value => {
    if (value) return undefined;
    return "Field is required"
}

export const maxLengthCreator = (maxLength) => (value) => {
    if(value.length > maxLength) {
        return `Max length ${maxLength} symbols`;
    }
    return undefined;
}

export const email = value => {
    return value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ?
        'Invalid email address' : undefined
}

export const number = value => {
    return value && isNaN(Number(value)) ? 'Must be a number' : undefined
}
